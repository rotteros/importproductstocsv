<?php

namespace LukaszGrabek\ImportProductsToCSV\Api;

/**
 * Interface WriteInterface
 * @package LukaszGrabek\ImportProductsToCSV\Api
 */
interface WriteInterface
{
    public function write(array $products);
}
