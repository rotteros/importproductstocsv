<?php

namespace LukaszGrabek\ImportProductsToCSV\Model;

use LukaszGrabek\ImportProductsToCSV\Api\WriteInterface;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Filesystem;

/**
 * Class CsvWriter
 * @package LukaszGrabek\ImportProductsToCSV\Model
 */
class CsvWriter implements WriteInterface
{
    /**
     * @var DirectoryList
     */
    private $directory;

    public function __construct(
        Filesystem $filesystem
    ) {
        $this->directory = $filesystem->getDirectoryWrite(DirectoryList::VAR_DIR);
    }

    public function write(array $products)
    {
        $filePath = 'export/productList.csv';
        $this->directory->create('export');
        $stream = $this->directory->openFile($filePath, 'w+');
        $stream->lock();

        $header = ['Id', 'Name'];
        $stream->writeCsv($header);

        foreach ($products as $product) {
            $data = [];
            $data[] = $product->getId();
            $data[] = $product->getName();
            $stream->writeCsv($data);
        }
    }
}
