<?php

namespace LukaszGrabek\ImportProductsToCSV\Cron;

use LukaszGrabek\ImportProductsToCSV\Api\WriteInterface;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;

/**
 * Class Job
 * @package LukaszGrabek\ImportProductsToCSV\Cron
 */
class Job
{
    /**
     * @var WriteInterface
     */
    private $writeInterface;

    /**
     * @var CollectionFactory
     */
    private $collectionFactory;

    public function __construct(
        WriteInterface $writeInterface,
        CollectionFactory $collectionFactory
    ) {
        $this->writeInterface = $writeInterface;
        $this->collectionFactory = $collectionFactory;
    }

    public function execute()
    {
        $this->writeInterface->write($this->getProductCollection());
    }

    public function getProductCollection()
    {
        $productCollection = $this->collectionFactory->create();
        $productCollection->addAttributeToSelect('*');

        return $productCollection;
    }
}
