<?php

use Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(ComponentRegistrar::MODULE, 'LukaszGrabek_ImportProductsToCSV', __DIR__);
